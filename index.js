// console.log("Hello World");

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function printWelcomeMessage(){
		let fullName = prompt("What is your name?");
		let age = prompt("How old are you?");
		let location = prompt("Where do you live?");
		
		console.log("Hello, " +fullName);
		console.log("You are " +age+ " years old.");
		console.log("You live in " +location+ ".");
		alert("Thank you for your input!");
	}

	printWelcomeMessage();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function displayFaveArtist(){
		let fave1 = "1. Lin Manuel Miranda";
		let fave2 = "2. Jason Mraz";
		let fave3 = "3. Ed Sheeran";
		let fave4 = "4. Panic At The Disco";
		let fave5 = "5. The Script";
		// let favorites = ["The Script", "Jason Mraz", "Ed Sheeran", "Panic At The Disco", "Lin Manuel Miranda"];
		console.log(fave1);
		console.log(fave2);
		console.log(fave3);
		console.log(fave4);
		console.log(fave5);
	}
	displayFaveArtist();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function displayFaveMovies(){
		const movie1 = "1. Thor: Love and Thunder";
		let rating1 = "Rotten Tomatoes Rating: 65%";

		const movie2 = "2. Spider-Man: No Way Home: 93%";
		let rating2 = "Rotten Tomatoes Rating: 93%";

		const movie3 = "3. Avengers: Endgame: 94%";
		let rating3 = "Rotten Tomatoes Rating: 94%";

		const movie4 = "4. Avengers: Infinity War: 85%";
		let rating4 = "Rotten Tomatoes Rating: 85%";

		const movie5 = "5. Avengers: Age Of Ultron: 76%";
		let rating5 = "Rotten Tomatoes Rating: 76%";

		console.log(movie1);
		console.log(rating1);
		console.log(movie2);
		console.log(rating2);
		console.log(movie3);
		console.log(rating3);
		console.log(movie4);
		console.log(rating4);
		console.log(movie5);
		console.log(rating5);
	
	}
	displayFaveMovies();


/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();



// console.log(friend1);
// console.log(friend2);